<?php
// Helper functions are goes here

function debug($data, $is_exit = false)
{
    echo "<pre style='background:#ffffff;'>";
    print_r($data);
    echo "</pre>";
    if ($is_exit) {
        exit;
    }
}

function setSession($s_key, $s_value){
    if(!isset($_SESSION)){
        session_start();
    }
    $_SESSION[$s_key] = $s_value;
}
function redirect ($path, $key= null, $msg = null){
    if($key !== null){
        setSession($key, $msg);
    }
    header("location: ".$path);
    exit;
}

function flash(){
    if(isset($_SESSION['success']) && !empty($_SESSION['success'])){
        echo "<p class='alert alert-success'>".$_SESSION['success']."</p>";
        unset($_SESSION['success']);
    }

    if(isset($_SESSION['error']) && !empty($_SESSION['error'])){
        echo "<p class='alert alert-danger'>".$_SESSION['error']."</p>";
        unset($_SESSION['error']);
    }

    if(isset($_SESSION['warning']) && !empty($_SESSION['warning'])){
        echo "<p class='alert alert-warnings'>".$_SESSION['warning']."</p>";
        unset($_SESSION['warning']);
    }
}


function generateRandom($length = 100){
    $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $len_str = strlen($chars);
    $random = "";

    for($i=0; $i < $length; $i++){
       // $posn = rand(0, $len_str - 1);
        //$random .= "$chars[$posn];
        $random .= $chars[rand(0, $len_str - 1)];
    }
    return $random;
}

function sanitize($str){
    $str = strip_tags($str); // <p>value</p> => name value
    $str = rtrim($str);
    return $str;
}


function uploadImage($image, $dir){
    if($image['error'] == 0){

        if($image['size'] <= 90000000){
            $ext = pathinfo($image['name'], PATHINFO_EXTENSION);
            if(in_array(strtolower($ext), IMAGE_EXTENSIONS)){
                $image_name = ucfirst(strtolower($dir))."-".date(YmdHis).rand(0, 999).".".$ext;

                $upload_dir = UPLOAD_DIR."/".$dir;
                if(!is_dir($upload_dir)){
                    mkdir($upload_dir, 0777, true);
                }

                $status = move_uploaded_file($image['tmp_name'], $upload_dir."/".$image_name);
                if($status){
                    return $image_name;
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
            return null;
        }
    }else{
        return null;
    }
}

function deleteImage($image_name, $dir){
    $path = UPLOAD_DIR."/".$dir."/".$image_name;
    if($image_name != "" && file_exists($path) ){
        unlink($path);
    }
}


function getYouTubeVideoId($url){
    preg_match('/(?im)\b(?:https?:\/\/)?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)\/(?:(?:\??v=?i?=?\/?)|watch\?vi?=|watch\?.*?&v=|embed\/|)([A-Z0-9_-]{11})\S*(?=\s|$)/', $url, $return);
    if(isset($return[1])){
        return $return[1];
    }else{
        return null;
    }
}



