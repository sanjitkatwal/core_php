<?php
ob_start();
session_start();

define("SITE_URL", "http://corephp.me");
//define("SITE_URL", "http://localhost/php/php_project")

define("CMS_URL", SITE_URL . "/cms");

define("CLASS_PATH", $_SERVER['DOCUMENT_ROOT'] . "/class/");
define("ERROR_LOG", $_SERVER['DOCUMENT_ROOT'] . '/error/error.log');

define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "php_core");

define("IMAGES_URL", SITE_URL."/images");
define("IMAGE_EXTENSIONS", array('jpg', 'jpeg', 'gif', 'bmp', 'png', 'webp'));


define("UPLOAD_DIR", $_SERVER['DOCUMENT_ROOT']."/uploads");
define("UPLOAD_URL", SITE_URL."/uploads");

/** ADMIN Portal URLS */
define("CMS_ASSETS_URL", CMS_URL . '/assets');
define("CMS_CSS_URL", CMS_ASSETS_URL . '/css');
define("CMS_JS_URL", CMS_ASSETS_URL . '/js');

define("CMS_SITE_TITLE", "admin Portal || Blog Web");



$ads_pos = array(
    'home_1'    => "Above Menu",
    'home_2'    => "Home page Below Menu",
    'home_3'    => "Home page Below First Blog",
    'home_4'    => "Home page Below First Category",
    'home_5'    => "Home page Below Video section",
    'list_1'    =>"Listing Below Menu",
    'list_2'    => "Listing Below List Blogs",
    'detail_1'  => "Above Details Title",
    'detail_2'  => "After Detail Description"

);