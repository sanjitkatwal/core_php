<?php
require_once "../config/init.php";
$_title = "Page,".CMS_SITE_TITLE;
require_once "inc/header.php";
require "inc/checklogin.php";
?>

<link rel="stylesheet" href="<?php echo CMS_ASSETS_URL.'/lightbox/css/lightbox.min.css' ?>">
<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include "inc/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <?php include "inc/top-nav.php"; ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">
                    Page List

                </h1>
                <?php echo flash(); ?>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-sm table bordered table-hover">
                            <thead class="thead-dark">
                            <th>S.N</th>
                            <th>Title</th>
                            <th>Summary</th>
                            <th>Thumbnail</th>
                            <th>Status</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            <?php
                            $page = new Page;
                            //$category = new Category();
                            $data = $page->getAllRows();
                            if($data){
                                foreach ($data as $key => $page_value){
                                    ?>
                                    <tr>
                                        <td><?php echo $key+1; ?></td>
                                        <td><?php echo ucfirst($page_value->title); ?></td>
                                        <td><?php echo ucfirst($page_value->summary); ?></td>
                                        <td>
                                            <?php
                                            if($page_value->image && !empty($page_value->image)) {
                                                ?>
                                                <a href="<?php echo UPLOAD_URL . "/page/" . $age_value->image ?>" data-lightbox="page-<?php echo $page_value->id; ?>" data-title="<?php echo $page_value->title; ?>>">Preview</a>
                                                <?php
                                            }else{
                                                echo "NO IMAGE";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <span class="badge badge-<?php echo ($page_value->status == 'active') ? 'success':'danger'; ?>">
                                                <?php echo ($page_value->status == 'active') ? "Published" : "Un-published"; ?>
                                            </span>
                                        </td>
                                        <td>
                                            <a href="page-form.php?id=<?php echo $page_value->id; ?>" class="btn btn-sm btn-success btn-circle">
                                                <i class="fa fa-pen"></i>
                                            </a>
                                        </td>
                                    </tr>

                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php include "inc/copyright.php"; ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>



<?php include_once 'inc/footer.php'; ?>

<script src="<?php echo CMS_ASSETS_URL.'/lightbox/js/lightbox.min.js' ?>"></script>