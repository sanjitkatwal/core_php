<?php
require_once "../config/init.php";
$_title = "Blog form,".CMS_SITE_TITLE;
require_once "inc/header.php";
require "inc/checklogin.php";

$category = new Blog;
if(isset($_GET['id']) && !empty($_GET['id'])) {
    $id = (int)$_GET['id'];
    if ($id <= 0) {
        redirect("blog.php", "error", "Sorry, cateogry ID is invalid");
    }
    $blog_info = $blog->getRowById($id);
    if (!$blog_info) {
        redirect("blog.php", "error", "Sorry, cateogry might have been deleted or does not exist.");
    }
}
?>

<link rel="stylesheet" href=" <?php echo CMS_ASSETS_URL.'/summernote/summernote-bs4.min.css' ?>" >
<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include "inc/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <?php include "inc/top-nav.php"; ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">
                    Blog Form
                    <a href="blog-form.php" class="bt btn-success btn-sm float-right">
                        <i class="fa fa-plus">Blog Form</i>
                    </a>
                </h1>
                <?php echo flash(); ?>
                <div class="row">
                    <div class="col-12">
                        <form action="process/blog.php" method="post" enctype="multipart/form-data" class="form">
                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Title:</label>
                                <div class="col-sm-12 col-md-9">
                                    <input type="text" name="title" value="<?php echo @$blog_info[0]->title ?>" required placeholder="Enter Blog Title" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Summary:</label>
                                <div class="col-sm-12 col-md-9">
                                    <textarea name="summary" id="summary" cols="" rows="5" style="resize: none;" class="form-control form-control-sm"><?php echo @$blog_info[0]->summary ?></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Description:</label>
                                <div class="col-sm-12 col-md-9">
                                    <textarea name="description" id="description" cols="" rows="5" style="resize: none;" class="form-control form-control-sm"><?php echo @$blog_info[0]->description ?></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">status:</label>
                                <div class="col-sm-12 col-md-9">
                                    <select name="status" id="status" class="form-control form-control-sm" required>
                                        <option value="active" <?php echo (isset($blog_info) && $blog_info[0]->status == 'active') ? 'selected': '' ?> >Published</option>
                                        <option value="inactive" <?php echo (isset($blog_info) && $blog_info[0]->status == 'inactive') ? 'selected': '' ?> >Un-published</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Category:</label>
                                <div class="col-sm-12 col-md-9">
                                    <select name="category_id" id="status" class="form-control form-control-sm" required>
                                        <option value="" selected disabled>-- Select Any one --</option>
                                        <?php
                                        $category = new Category();
                                        $cat = $category->getActiveCategory();
                                        if($cat){
                                            foreach ($cat as $cat_info){
                                               ?>
                                                <option value="<?php echo $cat_info->id; ?>"><?php echo $cat_info->title; ?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Featured:</label>
                                <div class="col-sm-12 col-md-9">
                                    <input type="checkbox" name="is_featured" value="1" checked> Yes
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Image:</label>
                                <div class="col-sm-12 col-md-9">
                                    <input type="file" name="image" accept="image/*" class="form-control form-control-sm">
                                </div>

                                <div class="cod-sm-12 col-md-2">
                                    <?php
                                    if(isset($blog_info) && !empty($blog_info[0]->image) && file_exists(UPLOAD_DIR.'/blog/'.$blog_info[0]->image)){
                                        ?>
                                        <img src="<?php echo UPLOAD_URL.'/blog/'.$blog_info[0]->image ?>" alt="" class="img img-fluid" width="100" height="100" style="float: right">
                                        <?php
                                    }
                                    ?>
                                </div>


                            </div>

                            <div class="form-group row">
                                <div class="offset-md-3 col-sm-12 col-md-9">
                                    <input type="hidden" name="blog_id" value="<?php echo @$blog_info[0]->id; ?>">
                                    <button class="btn-sm btn-danger" type="reset">
                                        <i class="fa fa-items"></i>Reset
                                    </button>

                                    <button class="btn-sm btn-success" type="submit">
                                        <i class="fa fa-paper-plane"></i>Submit
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php include "inc/copyright.php"; ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>



<?php include_once 'inc/footer.php'; ?>
<script src="<?php echo CMS_ASSETS_URL.'/summernote/summernote-bs4.min.js' ?>"></script>
<script>
    $('#description').summernote({
        height: 200
    });
</script>