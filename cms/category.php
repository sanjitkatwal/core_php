<?php
require_once "../config/init.php";
$_title = "Category,".CMS_SITE_TITLE;
require_once "inc/header.php";
require "inc/checklogin.php";
?>

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include "inc/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <?php include "inc/top-nav.php"; ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">
                    Category List
                    <a href="category-form.php" class="bt btn-success btn-sm float-right">
                        <i class="fa fa-plus">Category Add</i>
                    </a>
                </h1>
                <?php echo flash(); ?>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-sm table bordered table-hover">
                            <thead class="thead-dark">
                            <th>S.N</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                                <?php
                                $category = new Category;
                                $data = $category->getAllRows();
                                if($data){
                                    foreach ($data as $key => $cat_value){
                                        ?>
                                        <tr>
                                            <td><?php echo $key+1; ?></td>
                                            <td><?php echo ucfirst($cat_value->title); ?></td>
                                            <td>
                                                <?php
                                                if($cat_value->image != null) {
                                                    ?>
                                                    <img src="<?php echo UPLOAD_URL . "/category/" . $cat_value->image ?>"
                                                         alt="" width="150" height="150">
                                                    <?php
                                                }else{
                                                    echo "NO IMAGE";
                                                }
                                                    ?>
                                            </td>
                                            <td>
                                                <span class="badge badge-<?php echo ($cat_value->status == 'active') ? 'success':'danger'; ?>">
                                                    <?php echo ($cat_value->status == 'active') ? "Published" : "Un-published"; ?>
                                                </span>
                                            </td>
                                            <td><?php echo date("Y-m-d", strtotime($cat_value->created_at)); ?></td>
                                            <td>
                                                <a href="category-form.php?id=<?php echo $cat_value->id; ?>" class="btn btn-sm btn-success btn-circle">
                                                    <i class="fa fa-pen"></i>
                                                </a>
                                                <a href="process/category.php?id=<?php echo $cat_value->id; ?>" onclick="return confirm('Are you suer want to delete this category?');" class="btn btn-sm btn-danger btn-circle">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>

                                    <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php include "inc/copyright.php"; ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>



<?php include_once 'inc/footer.php'; ?>
