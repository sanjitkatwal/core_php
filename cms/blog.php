<?php
require_once "../config/init.php";
$_title = "Blog,".CMS_SITE_TITLE;
require_once "inc/header.php";
require "inc/checklogin.php";
?>

<link rel="stylesheet" href="<?php echo CMS_ASSETS_URL.'/lightbox/css/lightbox.min.css' ?>">
<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include "inc/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <?php include "inc/top-nav.php"; ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">
                    Blog List
                    <a href="blog-form.php" class="bt btn-success btn-sm float-right">
                        <i class="fa fa-plus">Blog Add</i>
                    </a>
                </h1>
                <?php echo flash(); ?>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-sm table bordered table-hover">
                            <thead class="thead-dark">
                            <th>S.N</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Summary</th>
                            <th>Image</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            <?php
                            $blog = new Blog;
                            //$category = new Category();
                            $data = $blog->getAllBlogs();
                            if($data){
                                foreach ($data as $key => $blog_value){
                                    ?>
                                    <tr>
                                        <td><?php echo $key+1; ?></td>
                                        <td><?php echo ucfirst($blog_value->title); ?></td>
                                        <!--<td><?php /*echo ($category->getRowById($blog_value->category_id))[0]->title; */?></td>-->
                                        <td><?php echo $blog_value->category_title; ?></td>
                                        <td><?php echo ucfirst($blog_value->summary); ?></td>
                                        <td>
                                            <?php
                                            if($blog_value->image && !empty($blog_value->image)) {
                                                ?>
                                                <a href="<?php echo UPLOAD_URL . "/blog/" . $blog_value->image ?>" data-lightbox="blog-<?php echo $blog_value->id; ?>" data-title="<?php echo $blog_value->title; ?>>">Preview</a>
                                                <?php
                                            }else{
                                                echo "NO IMAGE";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <span class="badge badge-<?php echo ($blog_value->status == 'active') ? 'success':'danger'; ?>">
                                                <?php echo ($blog_value->status == 'active') ? "Published" : "Un-published"; ?>
                                            </span>
                                        </td>
                                        <td><?php echo date("Y-m-d", strtotime($blog_value->created_at)); ?></td>
                                        <td>
                                            <a href="blog-form.php?id=<?php echo $blog_value->id; ?>" class="btn btn-sm btn-success btn-circle">
                                                <i class="fa fa-pen"></i>
                                            </a>
                                            <a href="process/blog.php?id=<?php echo $blog_value->id; ?>" onclick="return confirm('Are you suer want to delete this blog?');" class="btn btn-sm btn-danger btn-circle">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>

                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php include "inc/copyright.php"; ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>



<?php include_once 'inc/footer.php'; ?>

<script src="<?php echo CMS_ASSETS_URL.'/lightbox/js/lightbox.min.js' ?>"></script>