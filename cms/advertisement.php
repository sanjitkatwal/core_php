<?php
require_once "../config/init.php";
$_title = "Advertisements,".CMS_SITE_TITLE;
require_once "inc/header.php";
require "inc/checklogin.php";
?>


<link rel="stylesheet" href="<?php echo CMS_ASSETS_URL.'/lightbox/css/lightbox.min.css' ?>">
<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include "inc/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <?php include "inc/top-nav.php"; ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">
                    Advertisements List
                    <a href="advertisement-form.php" class="bt btn-success btn-sm float-right">
                        <i class="fa fa-plus">Advertisement Add</i>
                    </a>
                </h1>
                <?php echo flash(); ?>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-sm table bordered table-hover">
                            <thead class="thead-dark">
                            <th>S.N</th>
                            <th>Title</th>
                            <th>Link</th>
                            <th>Image</th>
                            <th>Status</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            <?php
                            $advertisement = new Advertisement;
                            $data = $advertisement->getAllRows();
                            if($data){
                                foreach ($data as $key => $advertisement_value){
                                    ?>
                                    <tr>
                                        <td><?php echo $key+1; ?></td>
                                        <td><?php echo ucfirst($advertisement_value->title); ?></td>
                                        <td><?php echo ucfirst($advertisement_value->link); ?></td>
                                        <td>
                                            <a href="<?php echo UPLOAD_URL.'/advertisement/'.$advertisement_value->image; ?>" data-lightbox="ads-<?php echo $advertisement_value->idl ?>" data-title="<?php echo $advertisement_value->title; ?>">Preview</a>
                                        </td>
                                        <td>
                                            <span class="badge badge-<?php echo ($advertisement_value->status == 'active') ? 'success':'danger'; ?>">
                                                <?php echo ($advertisement_value->status == 'active') ? "Published" : "Un-published"; ?>
                                            </span>
                                        </td>
                                        <td>
                                            <a href="advertisement-form.php?id=<?php echo $advertisement_value->id; ?>" class="btn btn-sm btn-success btn-circle">
                                                <i class="fa fa-pen"></i>
                                            </a>
                                            <a href="process/advertisement.php?id=<?php echo $advertisement_value->id; ?>" onclick="return confirm('Are you sure want to delete this Advertisement?');" class="btn btn-sm btn-danger btn-circle">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>

                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php include "inc/copyright.php"; ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>



<?php include_once 'inc/footer.php'; ?>

<script src="<?php echo CMS_ASSETS_URL.'/lightbox/js/lightbox.min.js' ?>"></script>