<?php
require_once "../config/init.php";
$_title = "Video form,".CMS_SITE_TITLE;
require_once "inc/header.php";
require "inc/checklogin.php";

$video = new Video;
if(isset($_GET['id']) && !empty($_GET['id'])) {
    $id = (int)$_GET['id'];
    if ($id <= 0) {
        redirect("category.php", "error", "Sorry, cateogry ID is invalid");
    }
    $video_info = $video->getRowById($id);
    if (!$video_info) {
        redirect("category.php", "error", "Sorry, cateogry might have been deleted or does not exist.");
    }
}
?>

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include "inc/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <?php include "inc/top-nav.php"; ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">
                    Video Form
                    <a href="category-form.php" class="bt btn-success btn-sm float-right">
                        <i class="fa fa-plus">Video Form</i>
                    </a>
                </h1>
                <?php echo flash(); ?>
                <div class="row">
                    <div class="col-12">
                        <form action="process/video.php" method="post" enctype="multipart/form-data" class="form">
                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Title:</label>
                                <div class="col-sm-12 col-md-9">
                                    <input type="text" name="title" value="<?php echo @$video_info[0]->title ?>" required placeholder="Enter Video Title" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Video Url(YouTube):</label>
                                <div class="col-sm-12 col-md-9">
                                    <input type="url" name="video_link" value="<?php echo @$video_info[0]->video_link ?>" required placeholder="Enter Video Link" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">status:</label>
                                <div class="col-sm-12 col-md-9">
                                    <select name="status" id="status" class="form-control form-control-sm" required>
                                        <option value="active" <?php echo (isset($video_info) && $video_info[0]->status == 'active') ? 'selected': '' ?> >Published</option>
                                        <option value="inactive" <?php echo (isset($video_info) && $video_info[0]->status == 'inactive') ? 'selected': '' ?> >Un-published</option>
                                    </select>
                                </div>
                            </div>

                                <div class="form-group row">
                                <div class="offset-md-3 col-sm-12 col-md-9">
                                    <input type="hidden" name="video_id" value="<?php echo @$video_info[0]->id; ?>">
                                    <button class="btn-sm btn-danger" type="reset">
                                        <i class="fa fa-items"></i>Reset
                                    </button>

                                    <button class="btn-sm btn-success" type="submit">
                                        <i class="fa fa-paper-plane"></i>Submit
                                    </button>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php include "inc/copyright.php"; ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>



<?php include_once 'inc/footer.php'; ?>
