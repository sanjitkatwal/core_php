<?php
require_once "../config/init.php";
$_title = "Gallery,".CMS_SITE_TITLE;
require_once "inc/header.php";
require "inc/checklogin.php";

$gallery_image = new GalleryImages;
?>
<link rel="stylesheet" href="<?php echo CMS_ASSETS_URL.'/lightbox/css/lightbox.min.css' ?>">
<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include "inc/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <?php include "inc/top-nav.php"; ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">
                    Gallery List
                    <a href="gallery-form.php" class="bt btn-success btn-sm float-right">
                        <i class="fa fa-plus">Gallery Add</i>
                    </a>
                </h1>
                <?php echo flash(); ?>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-sm table bordered table-hover">
                            <thead class="thead-dark">
                            <th>S.N</th>
                            <th>Title</th>
                            <th>Photographer</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                                <?php
                                $gallery = new Gallery;
                                $data = $gallery->getAllRows();
                                if($data){
                                    foreach ($data as $key => $gallery_info){
                                        ?>
                                        <tr>
                                            <td><?php echo $key+1; ?></td>
                                            <td><?php echo ucfirst($gallery_info->title); ?></td>
                                            <td><?php echo ucfirst($gallery_info->photographer); ?></td>
                                            <td>
                                                <span class="badge badge-<?php echo ($gallery_info->status == 'active') ? 'success':'danger'; ?>">
                                                    <?php echo ($gallery_info->status == 'active') ? "Published" : "Un-published"; ?>
                                                </span>
                                            </td>
                                            <td><?php echo date("Y-m-d", strtotime($gallery_info->created_at)); ?></td>
                                            <td>
                                                <?php
                                                    $images = $gallery_image->getImageByGalleryId($gallery_info->id);
                                                    if($images) {
                                                        foreach($images as $i => $gal_image){
                                                        ?>
                                                        <a href="<?php echo UPLOAD_URL . "/gallery/" . $gal_image->image_name ?>" data-lightbox="gallery-<?php echo $gallery_info->id; ?>" data-title="<?php echo $gallery_info->title; ?>>" class="<?php echo ($i ==0) ? 'btn btn-warning btn-circle btn-sm':''  ?>">
                                                           <?php if($i ==0){ ?>
                                                            <i class="fa fa-eye"></i>
                                                            <?php } ?>
                                                        </a>
                                                    <?php
                                                    }
                                                    }
                                                    ?>
                                                <a href="gallery-form.php?id=<?php echo $gallery_info->id; ?>" class="btn btn-sm btn-success btn-circle">
                                                    <i class="fa fa-pen"></i>
                                                </a>
                                                <a href="process/gallery.php?id=<?php echo $gallery_info->id; ?>" onclick="return confirm('Are you suer want to delete this Gallery?');" class="btn btn-sm btn-danger btn-circle">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>

                                    <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php include "inc/copyright.php"; ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>



<?php include_once 'inc/footer.php'; ?>

<script src="<?php echo CMS_ASSETS_URL.'/lightbox/js/lightbox.min.js' ?>"></script>
