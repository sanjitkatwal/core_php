<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &nbsp; &copy; Blog Cms <?php echo date("Y") != "2020" ? "2020-".date("Y") : date("Y"); ?></span>
        </div>
    </div>
</footer>