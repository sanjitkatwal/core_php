<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
            <i class="fas fa-home"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Blog CMS</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="dashboard.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Interface
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="category.php">
            <i class="fas fa-fw fa-sitemap"></i>
            <span>Category Manager</span>
        </a>
    </li>
    <hr class="sidebar-divider d-none d-md-block">

    <li class="nav-item">
        <a class="nav-link collapsed" href="blog.php">
            <i class="fas fa-fw fa-newspaper"></i>
            <span>Blog Manager</span>
        </a>
    </li>
    <hr class="sidebar-divider d-none d-md-block">

    <li class="nav-item">
                <a class="nav-link collapsed" href="gallery.php">
            <i class="fas fa-fw fa-images"></i>
            <span>Gallery Manager</span>
        </a>
    </li>
    <hr class="sidebar-divider d-none d-md-block">

    <li class="nav-item">
        <a class="nav-link collapsed" href="video.php">
            <i class="fas fa-fw fa-video"></i>
            <span>Video Manager</span>
        </a>
    </li>
    <hr class="sidebar-divider d-none d-md-block">

    <li class="nav-item">
        <a class="nav-link collapsed" href="advertisement.php">
            <i class="fas fa-fw fa-ad"></i>
            <span>Advertisement Manager</span>
        </a>
    </li>
    <hr class="sidebar-divider d-none d-md-block">

    <li class="nav-item">
        <a class="nav-link collapsed" href="user.php">
            <i class="fas fa-fw fa-user"></i>
            <span>User Manager</span>
        </a>
    </li>
    <hr class="sidebar-divider d-none d-md-block">

    <li class="nav-item">
        <a class="nav-link collapsed" href="pages.php">
            <i class="fas fa-fw fa-cog"></i>
            <span>Pages Manager</span>
        </a>
    </li>
    <hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>
</ul>