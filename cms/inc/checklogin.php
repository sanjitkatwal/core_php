<?php

if(!isset($_SESSION, $_SESSION['token']) || empty($_SESSION['token']) ){
    if(!isset($_COOKIE, $_COOKIE['_au']) || empty($_COOKIE['_au'])) {
        redirect('./', 'error', 'Please Login First.');
    }else{
       // debug($_COOKIE, true);
        $token = $_COOKIE['_au'];

        $user = new User();
        $user_info = $user->getUserByCookie($token);
        //debug($user_info, true);

        if($user_info){
            //cookie exist
            setSession('user_id', $user_info[0]->id);
            setSession('name', $user_info[0]->name);
            setSession('email', $user_info[0]->email);
            setSession('image', $user_info[0]->image);

            $token = generateRandom(100);
            setSession('token', $token);

            //set cookie for remember me
            setcookie("_au", $token, time()+8640000, "/"); //set cookie need three parameter
            // Update logged in user
            $data = array(
                'remember_token' => $token
            );
            $user->updateRowById($data, $user_info[0]->id);

        }else{
            // This is incorrect cookie
            setcookie("_au", "", time()-60, "/");
            redirect("./", "error", "Unauthorized, Please clear your cookies before login.");
        }
    }
}