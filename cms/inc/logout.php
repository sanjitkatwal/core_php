<?php
require_once  "../../config/init.php";
require_once  "./checklogin.php";



// user database remember_token = null update
if(isset($_COOKIE['_au'])){
    setcookie("_au", "", time()-60, "/");
    $user = new User();
    $data = array(
        'remember_token' => ''
    );
    $user->updateRowById($data, $_SESSION['user_id']);
}

session_destroy();
redirect("../");