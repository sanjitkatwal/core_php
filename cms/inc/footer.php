<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="inc/logout.php">Logout</a>
            </div>
        </div>
    </div>
</div>
<!-- User Password Change Modal-->
<div class="modal fade" id="passwordChange" tabindex="-1" role="dialog" aria-labelledby="passwordChange" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="passwordChange">Change Password</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <form class="form" action="process/change_password.php" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="" class="col-sm-12 md-3">Password:</label>
                        <div class="col-sm-12 col-md-9">
                            <input type="password" name="password" required placeholder="Enter Your Name" id="password" class="form-control form-control-sm">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-12 md-3">Retype Password:</label>
                        <div class="col-sm-12 col-md-9">
                            <input type="password" name="re_password" required placeholder="Enter Your Name" id="re_password" class="form-control form-control-sm">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?>" id="change_user_id">
                    <button class="btn btn-danger" type="reset" data-dismiss="modal">
                        <i class="fa fa-times"></i> Cancel</button>
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-paper-plane"></i> Change Password</button>
                </div>
            </form>

        </div>
    </div>
</div>


<!-- Edit Profile Modal-->
<div class="modal fade" id="editProfile" tabindex="-1" role="dialog" aria-labelledby="editProfileLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editProfileLabel">Edit Your Profile</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form class="form" action="process/user.php" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="" class="col-sm-12 md-3">Name:</label>
                        <div class="col-sm-12 col-md-9">
                            <input type="text" name="name" required placeholder="Enter Your Name" value="<?php echo $_SESSION['name']; ?>" class="form-control form-control-sm">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="" class="col-sm-12 md-3">Image:</label>
                        <div class="col-sm-12 col-md-3">
                            <input type="file" name="image" accept="image/*" class="form-control form-control-sm">
                            <div class="col-sm-12 col-md-2">
                                <?php
                                if(isset($_SESSION['image']) && !empty($_SESSION['image'])){
                                    $url = UPLOAD_URL."/user/".$_SESSION['image'];
                                }else{
                                    $url = IMAGES_URL."/user/no-user.jpg";
                                }
                                ?>
                                <img  class="img img-fluid img-thumbnail" src="<?php echo $url; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" type="reset" data-dismiss="modal">
                        <i class="fa fa-times"></i> Cancel</button>
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-paper-plane"></i> Update</button>
                </div>
            </form>

        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?php echo CMS_JS_URL; ?>/jquery.js"></script>
<script src="<?php echo CMS_JS_URL; ?>/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo CMS_JS_URL; ?>/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?php echo CMS_JS_URL; ?>/sb-admin-2.min.js"></script>


<script>
    setTimeout(function(e){
        $('.alert').slideUp();
    }, 3000)

    $('.editprofile').click(function (e) {
        e.preventDefault();
        $("#editProfile").modal('show');
    });

    $('.password_change').click(function (e) {
        e.preventDefault();
        let user_id = $(this).data('user_id');
        $('#change_user_id').val(user_id);
        $("#passwordChange").modal('show');
    });
</script>
</body>

</html>