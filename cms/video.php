<?php
require_once "../config/init.php";
$_title = "Video,".CMS_SITE_TITLE;
require_once "inc/header.php";
require "inc/checklogin.php";
?>

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include "inc/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <?php include "inc/top-nav.php"; ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">
                    Video List
                    <a href="video-form.php" class="bt btn-success btn-sm float-right">
                        <i class="fa fa-plus">Video Add</i>
                    </a>
                </h1>
                <?php echo flash(); ?>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-sm table bordered table-hover">
                            <thead class="thead-dark">
                            <th>S.N</th>
                            <th>Title</th>
                            <th>Thumbnail</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            <?php
                            $video = new Video;
                            $data = $video->getAllRows();
                            if($data){
                                foreach ($data as $key => $video_value){
                                    ?>
                                    <tr>
                                        <td><?php echo $key+1; ?></td>
                                        <td><?php echo ucfirst($video_value->title); ?></td>
                                        <td>
                                            <a href="javascript:;" data-video_id="<?php echo $video_value->video_id ?>" class="btn btn-sm btn-link preview">Preview</a>
                                        </td>
                                        <td>
                                            <span class="badge badge-<?php echo ($video_value->status == 'active') ? 'success':'danger'; ?>">
                                                <?php echo ($video_value->status == 'active') ? "Published" : "Un-published"; ?>
                                            </span>
                                        </td>
                                        <td><?php echo date("Y-m-d", strtotime($video_value->created_at)); ?></td>
                                        <td>
                                            <a href="video-form.php?id=<?php echo $video_value->id; ?>" class="btn btn-sm btn-success btn-circle">
                                                <i class="fa fa-pen"></i>
                                            </a>
                                            <a href="process/video.php?id=<?php echo $video_value->id; ?>" onclick="return confirm('Are you suer want to delete this blog?');" class="btn btn-sm btn-danger btn-circle">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>

                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php include "inc/copyright.php"; ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Video Modal-->
<div class="modal fade" id="video-preview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <iframe width="100%" height="450" id="video_preview" src="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>

<?php include_once 'inc/footer.php'; ?>

<script>
    $('.preview').click(function(e){
        e.preventDefault();
        var video_id = $(this).data('video_id');
        let video_url = "https://www.youtube.com/embed/"+video_id;
        $('#video_preview').attr('src', video_url);

        $('#video-preview').modal('show');
    });
</script>