<?php
require_once "../config/init.php";
$_title = "User form,".CMS_SITE_TITLE;
require_once "inc/header.php";
require "inc/checklogin.php";

$user = new User;
if(isset($_GET['id']) && !empty($_GET['id'])) {
    $id = (int)$_GET['id'];
    if ($id <= 0) {
        redirect("user.php", "error", "Sorry, user ID is invalid");
    }
    $user_data = $user->getRowById($id);
    if (!$user_data) {
        redirect("user.php", "error", "Sorry, user might have been deleted or does not exist.");
    }
}
?>

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include "inc/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <?php include "inc/top-nav.php"; ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">
                    User Form
                    <a href="user-form.php" class="bt btn-success btn-sm float-right">
                        <i class="fa fa-plus">User Form</i>
                    </a>
                </h1>
                <?php echo flash(); ?>
                <div class="row">
                    <div class="col-12">
                        <form action="process/register.php" method="post" enctype="multipart/form-data" class="form">
                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Name:</label>
                                <div class="col-sm-12 col-md-9">
                                    <input type="text" name="name" value="<?php echo @$user_data[0]->name ?>" required placeholder="Enter User Name" class="form-control form-control-sm">
                                </div>
                            </div>

                            <?php if(!isset($user_data) || empty($user_data)){ ?>
                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Email:</label>
                                <div class="col-sm-12 col-md-9">
                                    <input type="email" name="email" value="<?php echo @$user_data[0]->email ?>" required placeholder="Enter User Email" class="form-control form-control-sm">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Password:</label>
                                <div class="col-sm-12 col-md-9">
                                    <input type="password" name="password" required placeholder="Enter User Password" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Re-password:</label>
                                <div class="col-sm-12 col-md-9">
                                    <input type="password" name="password_confirmation" required placeholder="Retyper password" class="form-control form-control-sm">
                                </div>
                            </div>
                            <?php } ?>

                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Status:</label>
                                <div class="col-sm-12 col-md-9">
                                    <select name="status" id="status" class="form-control form-control-sm" required>
                                        <option value="active" <?php echo (isset($user_data) && $user_data[0]->status == 'active') ? 'selected': '' ?> >Published</option>
                                        <option value="inactive" <?php echo (isset($user_data) && $user_data[0]->status == 'inactive') ? 'selected': '' ?> >Un-published</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Role:</label>
                                <div class="col-sm-12 col-md-9">
                                    <select name="role" id="role" class="form-control form-control-sm" required>
                                        <option value="admin" <?php echo (isset($user_data) && $user_data[0]->role == 'admin') ? 'selected': '' ?> >Admin</option>
                                        <option value="reporter" <?php echo (isset($user_data) && $user_data[0]->role == 'reporter') ? 'selected': '' ?> >Reporter</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Image:</label>
                                <div class="col-sm-12 col-md-9">
                                    <input type="file" name="image" accept="image/*" class="form-control form-control-sm">
                                </div>

                                <div class="cod-sm-12 col-md-2">
                                    <?php
                                    if(isset($user_data) && !empty($user_data[0]->image) && file_exists(UPLOAD_DIR.'/user/'.$user_data[0]->image)){
                                        ?>
                                        <img src="<?php echo UPLOAD_URL.'/user/'.$user_data[0]->image ?>" alt="" class="img img-fluid" width="100" height="100" style="float: right">
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="offset-md-3 col-sm-12 col-md-9">
                                    <input type="hidden" name="user_id" value="<?php echo @$user_data[0]->id; ?>">
                                    <button class="btn-sm btn-danger" type="reset">
                                        <i class="fa fa-items"></i>Reset
                                    </button>

                                    <button class="btn-sm btn-success" type="submit">
                                        <i class="fa fa-paper-plane"></i>Submit
                                    </button>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php include "inc/copyright.php"; ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>



<?php include_once 'inc/footer.php'; ?>
