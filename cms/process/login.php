<?php
require_once "../../config/init.php";

if(isset($_POST) && !empty($_POST)){
    $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
    if(!$email){
        redirect("../", 'error', "Incorrect email format.");
    }

    $user = new User();
    $user_info = $user->getUserByEmail($email);

    if($user_info){
        // user exist
        if(password_verify($_POST['password'], $user_info[0]->password)){
            if($user_info[0]->status == 'active'){
                //login allowed 
                if($user_info[0]->role == 'admin'){
                    // successful login
                    setSession('user_id', $user_info[0]->id);
                    setSession('name', $user_info[0]->name);
                    setSession('email', $user_info[0]->email);
                    setSession('image', $user_info[0]->image);

                    $token = generateRandom(100);
                    setSession('token', $token);

                    if(isset($_POST['remember_me'])){
                        //set cookie for remember me
                        setcookie("_au", $token, time()+8640000, "/"); //set cookie need three parameter
                        // Update logged in user
                        $data = array(
                          'remember_token' => $token
                        );
                        //UPDATE users SET remember_token = :remember_me WHERE id = :id
                        //UPDATE users SET password = :password WHERE id = :id
                        //UPDATE users SET name = :name, email =:email WHERE id = :id
                        $user->updateRowById($data, $user_info[0]->id);
                    }
                    redirect('../dashboard.php', 'success', 'Welcome to ADMIN panel.');
                }else{

                    redirect('../', 'warning', 'Your are not authorized to access.');
                }
            }else{
                redirect('../', 'error', 'your account has been dissabled.');
            }
        }else{
            redirect('../', 'error', 'Password Does not match');
        }
    }else{
        redirect('../', 'error', 'Invalid Credentials');
    }

    // "SELECT * FROM users WHERE email = :email "
    // "SELECT * FROM users WHERE role = :role "
    // "SELECT * FROM users WHERE id = :id "


    // fetch data from database


    // password_hash();
    // password_verify($_POST['password'], $hash);

} else{
    redirect("../", "error", "Please Login First.");
}