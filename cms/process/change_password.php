<?php
require_once "../../config/init.php";
require "../inc/checklogin.php";
$user = new User();
if(isset($_POST) && !empty($_POST)){
    if(($_POST['password'] !== $_POST['re_password']) || empty($_POST['password']) || empty($_POST['re_password'])){
        redirect("../dashboard.php", "error", "Password cannot be null or password does not match");
    }
    $data = array(
      'password' => password_hash($_POST['password'], PASSWORD_BCRYPT)
    );

    $referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ;
    if($referrer){
        $redirect = pathinfo($referrer, PATHINFO_FILENAME);
    }else{
        $redirect = "dashboard";
    }

    $user_id = $user->updateRowById($data, $_POST['user_id']);

    if($user_id){
        if($user_id == $_SESSION['user_id']){
            redirect("../inc/logout.php");
        }else{
            redirect('../'.$redirect.'.php', 'success', 'Password changed successfully.');
        }
    }else{
        redirect("../".$redirect.".php", "error", "Sorry! your information is could not be updated at this moment.");
    }
}else{
    redirect("../dashboard.php", "error", "Please update your data first.");
}