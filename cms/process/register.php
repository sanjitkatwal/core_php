<?php
require_once "../../config/init.php";
require "../inc/checklogin.php";

/*debug($_POST);
debug($_FILES);
exit();*/

$user = new User();
if(isset($_POST) && !empty($_POST)){
    $data = array(
        'name' => sanitize($_POST['name']),
        'email' => sanitize($_POST['password']),
        'role' => sanitize($_POST['role']),
        'status' => sanitize($_POST['status']),
    );

    if(isset($_POST['password'], $_POST['password_confirmation']) && ($_POST['password'] != $_POST['password_confirmation'])){
        redirect('../user-form.php', 'error', 'Password does not match.');
    }

    $user_id = isset($_POST['user_id']) && !empty($_POST['user_id']) ? (int)$_POST['user_id'] : null;
    if($user_id){
        $act = "updat";
        $user_info = $user->getRowById($user_id);
    }else{
        $act = "add";
        $data['email'] = sanitize($_POST[email]);
        $data['password'] = password_hash($_POST['password'], PASSWORD_BCRYPT);
    }
    if(isset($_FILES['image']) && $_FILES['image']['error'] == 0){
        $image_name = uploadImage($_FILES['image'], "user");
        if($image_name){
            $data['image'] = $image_name;
            if(isset($user_info) && !empty($user_info[0]->image)){
                deleteImage($user_info[0]->image, 'user');
            }
        }
    }
    if(isset($user_info)){
        $user_id = $user->updateRowById($data, $user_id);
    }else{
        $user_id = $user->insertData($data);
    }

    if($user_id){
        redirect("../user.php", "success", "User  ".$act."ed successfully.");
    }else{
        redirect("../user.php", "error", "Sorry! your information is could not be ".$act."ed at this moment.");
    }
}
if(isset($_GET, $_GET['id']) && !empty($_GET['id'])){
    $id = (int)$_GET['id'];
    if($id <= 0){
        redirect('../user.php', 'error', 'Invalid Id');
    }
    $user_info = $user->getRowById($id);
    if(!$user_info){
        redirect('../user.php', 'error', 'Sorry! user not found');
    }
    $del = $user->deleteRowById($id);
    if($del){
        deleteImage($user_info[0]->image, "image");
        redirect('../user.php', 'success', 'User deleted successfully');
    }else{
        redirect('../user.php', 'error', 'There was problem while deleting the user.');
    }
}
else{
    redirect("../user-form.php", "error", "Please fill the form first.");
}