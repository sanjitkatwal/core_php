<?php
require_once "../../config/init.php";
require "../inc/checklogin.php";


$user = new User();
if(isset($_POST) && !empty($_POST)){
    $data = array(
        'name' => sanitize($_POST['name'])
    );

    if(isset($_FILES['image']) && $_FILES['image']['error'] == 0){
        $image_name = uploadImage($_FILES['image'], "user");
        if($image_name){
            $data['image'] = $image_name;
        }
    }
    $user_id = $user->updateRowById($data, $_SESSION['user_id']);
    if($user_id){
        setSession('name', $data['name']);
        setSession('image', $data['image']);

        redirect("../dashboard.php", "success", "Your profile updated successfully.");
    }else{
        redirect("../dashboard.php", "error", "Sorry! your information is could not be updated at this moment.");
    }
}else{
    redirect("../dashboard.php", "error", "Please update your data first.");
}