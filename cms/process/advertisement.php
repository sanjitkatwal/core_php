<?php

require_once "../../config/init.php";
require_once "../inc/checklogin.php";

//debug($_POST);
//debug($_FILES);
//exit();
$advertisement = new Advertisement;

if(isset($_POST) && !empty($_POST)){

    if(empty($_POST['title'])){
    redirect("../advertisement-form.php", "error", "Title is required.");
    }
    $data = array(
        'title'  => sanitize($_POST['title']),
        'link' => sanitize($_POST['link']),
        'end_date' => sanitize($_POST['end_date']),
        'position' => sanitize($_POST['position']),
        'status' =>sanitize($_POST['status']),
    );

    $ads_id = isset($_POST['ads_id']) ? (int) $_POST['ads_id']:null;
    if($ads_id == null){
        $act = "add";
        $data['created_by'] = $_SESSION['user_id'];
    }else{
        $act = "updat";
        $ads_info = $advertisement->getRowById($ads_id);
    }


    if(isset($_FILES['image']) && $_FILES['image']['error'] ==0){

        $image_name = uploadImage($_FILES['image'], "advertisement");
        if($image_name){
            $data['image'] = $image_name;

            if($ads_info[0]->image != null){
                deleteImage($ads_info[0]->image, 'advertisement');
            }
        }
    }

    //$row_id = $advertisement->insertData($data);

    if($act == 'add'){
        $row_id = $advertisement->insertData($data);
    }else{
        $row_id = $advertisement->updateRowById($data, $ads_id);
    }

    if($row_id){
        redirect("../advertisement.php", "success", "Advertisement ".$act. "ed successfully.");
    }else{
        redirect("../advertisement.php", "error", "Sorry there was a problem while ".$act."ing advertisement.");
    }

}elseif(isset($_GET['id']) && !empty($_GET['id'])){
    $id = (int)$_GET['id'];
    if ($id <=0){
        redirect("../advertisement.php", "error", "Sorry, advertisement ID is invalid");
    }
    $ads_info = $advertisement->getRowById($id);
    if(!$ads_info){
        redirect("../advertisement.php", "error", "Sorry, advertisement might have been deleted or does not exist.");
    }

    $status = $advertisement->deleteRowById($id);

    if($status){
        if($ads_info[0]->image != null){
            deleteImage($ads_info[0]->image, "advertisement");
        }
        //delete success
        redirect("../advertisement.php", "success", "Advertisement deleted successfully.");

    }else{
        // delete failure
        redirect("../advertisement.php", "error", "Sorry, there was a problem while deleting file");
    }
}
else{
    redirect("../advertisement-form.php", "error", "Please fill the form first");
}