<?php

require_once "../../config/init.php";
require_once "../inc/checklogin.php";

//debug($_POST);
//debug($_FILES);
//exit();

$gallery = new Gallery;
$gallery_image = new GalleryImages;

if(isset($_POST) && !empty($_POST)){

    if(empty($_POST['title'])){
    redirect("../gallery-form.php", "error", "Title is required.");
    }
    $data = array(
        'title'         => sanitize($_POST['title']),
        'summary'       => sanitize($_POST['summary']),
        'photographer'  => sanitize($_POST['photographer']),
        'status'        =>sanitize($_POST['status']),
    );

    $gallery_id = isset($_POST['gallery_id']) ? (int) $_POST['gallery_id']:null;
    if($gallery_id == null){
        $act = "add";
        $data['created_by'] = $_SESSION['user_id'];
    }else{
        $act = "updat";
        $gallery_info = $gallery->getRowById($gallery_id);
    }


    if($act == 'add'){
        $row_id = $gallery->insertData($data);
    }else{
        $row_id = $gallery->updateRowById($data, $gallery_id);
    }

    if($row_id){
        if(isset($_FILES['image']) && !empty($_FILES['image'])){
            $count = count($_FILES['image']['name']);

            for($i=0; $i < $count; $i++){
                $temp = array(
                  'name'        =>$_FILES['image']['name'][$i],
                  'type'        =>$_FILES['image']['type'][$i],
                  'tmp_name'    =>$_FILES['image']['tmp_name'][$i],
                  'error'       =>$_FILES['image']['error'][$i],
                  'size'        =>$_FILES['image']['size'][$i],
                );
                $image_name = uploadImage($temp, 'gallery');
                //debug($image_name, true);
                if($image_name){
                    $gallery_data = array(
                        'gallery_id'    => $row_id,
                        'image_name'    => $image_name,
                    );
                    $gall_image= new GalleryImages;
                    $gall_image->insertData($gallery_data);

                }
            }
        }

        if(isset($_POST['del_image']) && !empty($_POST['del_image'])){
            foreach ($_POST['del_image'] as $del_image){
                $del = $gallery_image->deleteImageByName($del_image);
                if ($del){
                    deleteImage($del_image, "gallery");
                }
            }
        }
        redirect("../gallery.php", "success", "Gallery ".$act. "ed successfully.");
    }else{
        redirect("../gallery.php", "error", "Sorry there was a problem while ".$act."ing category.");
    }

}elseif(isset($_GET['id']) && !empty($_GET['id'])){
    $id = (int)$_GET['id'];
    if ($id <=0){
        redirect("../gallery.php", "error", "Sorry, Gallery ID is invalid");
    }
    $gallery_info = $gallery->getRowById($id);
    if(!$gallery_info){
        redirect("../gallery.php", "error", "Sorry, Gallery might have been deleted or does not exist.");
    }

    $images = $gallery_image->getImageByGalleryId($id);
    $status = $gallery->deleteRowById($id);

    if($status){
        if($images != null){
            foreach ($images as $image_name){
                deleteImage($image_name->image_name, 'gallery');
            }
        }
        //delete success
        redirect("../gallery.php", "success", "Gallery deleted successfully.");

    }else{
        // delete failure
        redirect("../gallery.php", "error", "Sorry, there was a problem while deleting file");
    }
}
else{
    redirect("../gallery-form.php", "error", "Please fill the form first");
}