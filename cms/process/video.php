<?php

require_once "../../config/init.php";
require_once "../inc/checklogin.php";

$video = new Video;

if(isset($_POST) && !empty($_POST)){

    if(empty($_POST['title']) || empty($_POST['video_link'])){
        redirect("../video-form.php", "error", "Title is required.");
    }
    $data = array(
        'title'         => sanitize($_POST['title']),
        'video_link'    => sanitize($_POST['video_link']),
        'video_id'      => getYouTubeVideoId($_POST['video_link']),
        'status'        =>sanitize($_POST['status']),
    );

    if($data['video_id'] == null){
        redirect("../video-form.php", 'error', 'Video url should be of YouTube only.');
    }

    $video_id = isset($_POST['video_id']) ? (int) $_POST['video_id']:null;
    if($video_id == null){
        $act = "add";
        $data['created_by'] = $_SESSION['user_id'];
    }else{
        $act = "updat";
        $video_info = $video->getRowById($video_id);
    }

    //$row_id = $video->insertData($data);

    if($act == 'add'){
        $row_id = $video->insertData($data);
    }else{
        $row_id = $video->updateRowById($data, $video_id);
    }

    if($row_id){
        redirect("../video.php", "success", "Video ".$act. "ed successfully.");
    }else{
        redirect("../video.php", "error", "Sorry there was a problem while ".$act."ing video.");
    }

}elseif(isset($_GET['id']) && !empty($_GET['id'])){
    $id = (int)$_GET['id'];
    if ($id <=0){
        redirect("../video.php", "error", "Sorry, video ID is invalid");
    }
    $cat_info = $video->getRowById($id);
    if(!$cat_info){
        redirect("../video.php", "error", "Sorry, cateogry might have been deleted or does not exist.");
    }

    $status = $video->deleteRowById($id);

    if($status){
        //delete success
        redirect("../video.php", "success", "Video deleted successfully.");

    }else{
        // delete failure
        redirect("../video.php", "error", "Sorry, there was a problem while deleting file");
    }
}
else{
    redirect("../video-form.php", "error", "Please fill the form first");
}