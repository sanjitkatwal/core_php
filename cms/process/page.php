<?php

require_once "../../config/init.php";
require_once "../inc/checklogin.php";

//debug($_POST);
//debug($_FILES);
//exit();
$page = new Page;

if(isset($_POST) && !empty($_POST)){

    if(empty($_POST['title']) || empty($_POST['page_id']) || empty($_POST['summary'])){
        redirect("../page-form.php", "error", "Title or page or summary fields are required.");
    }
    $data = array(
        'title'         => sanitize($_POST['title']),
        'summary'       => sanitize($_POST['summary']),
        'description'   => htmlentities($_POST['description']),
        'status'        =>sanitize($_POST['status']),
    );

    $page_id = isset($_POST['page_id']) ? (int) $_POST['page_id'] : null;
    $act = "updat";
    $page_info = $page->getRowById($page_id);


    if(isset($_FILES['image']) && $_FILES['image']['error'] ==0){

        $image_name = uploadImage($_FILES['image'], "page");
        if($image_name){
            $data['image'] = $image_name;

            if($page_info[0]->image != null){
                deleteImage($page_info[0]->image, 'page');
            }
        }
    }

    $row_id = $page->updateRowById($data, $page_id);

    debug($row_id);
    exit();
    if($row_id){
        redirect("../page.php", "success", "Page ".$act. "ed successfully.");
    }else{
        redirect("../page.php", "error", "Sorry there was a problem while ".$act."ing page.");
    }

}else{
    redirect("../page.php", "error", "Please fill the form first");
}