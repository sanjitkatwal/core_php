<?php

require_once "../../config/init.php";
require_once "../inc/checklogin.php";

//debug($_POST);
//debug($_FILES);
//exit();
$category = new Category;

if(isset($_POST) && !empty($_POST)){

    if(empty($_POST['title'])){
    redirect("../category-form.php", "error", "Title is required.");
    }
    $data = array(
        'title'  => sanitize($_POST['title']),
        'summary' => sanitize($_POST['summary']),
        'status' =>sanitize($_POST['status']),
        //'created_by'  => $_SESSION['user_id']
    );

    $cat_id = isset($_POST['cat_id']) ? (int) $_POST['cat_id']:null;
    if($cat_id == null){
        $act = "add";
        $data['created_by'] = $_SESSION['user_id'];
    }else{
        $act = "updat";
        $cat_info = $category->getRowById($cat_id);
    }


    if(isset($_FILES['image']) && $_FILES['image']['error'] ==0){

        $image_name = uploadImage($_FILES['image'], "category");
        if($image_name){
            $data['image'] = $image_name;

            if($cat_info[0]->image != null){
                deleteImage($cat_info[0]->image, 'category');
            }
        }
    }

    //$row_id = $category->insertData($data);

    if($act == 'add'){
        $row_id = $category->insertData($data);
    }else{
        $row_id = $category->updateRowById($data, $cat_id);
    }

    if($row_id){
        redirect("../category.php", "success", "Category ".$act. "ed successfully.");
    }else{
        redirect("../category.php", "error", "Sorry there was a problem while ".$act."ing category.");
    }

}elseif(isset($_GET['id']) && !empty($_GET['id'])){
    $id = (int)$_GET['id'];
    if ($id <=0){
        redirect("../category.php", "error", "Sorry, cateogry ID is invalid");
    }
    $cat_info = $category->getRowById($id);
    if(!$cat_info){
        redirect("../category.php", "error", "Sorry, cateogry might have been deleted or does not exist.");
    }

    $status = $category->deleteRowById($id);

    if($status){
        if($cat_info[0]->image != null){
            deleteImage($cat_info[0]->image, "category");
        }
        //delete success
        redirect("../category.php", "success", "Category deleted successfully.");

    }else{
        // delete failure
        redirect("../category.php", "error", "Sorry, there was a problem while deleting file");
    }
}
else{
    redirect("../category-form.php", "error", "Please fill the form first");
}