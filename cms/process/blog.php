<?php

require_once "../../config/init.php";
require_once "../inc/checklogin.php";

//debug($_POST);
//debug($_FILES);
//exit();
$blog = new Blog;

if(isset($_POST) && !empty($_POST)){

    if(empty($_POST['title']) || empty($_POST['category_id']) || empty($_POST['summary'])){
    redirect("../blog-form.php", "error", "Title or blog or summary fields are required.");
    }
    $data = array(
        'title'         => sanitize($_POST['title']),
        'summary'       => sanitize($_POST['summary']),
        'description'   => htmlentities($_POST['description']),
        'category_id'   => sanitize($_POST['category_id']),
        'is_featured'   => (isset($_POST['blog_id']) ? (int) $_POST['blog_id'] : null),
        'status'        =>sanitize($_POST['status']),
        //'created_by'  => $_SESSION['user_id']
    );

    $blog_id = isset($_POST['blog_id']) ? (int) $_POST['blog_id']:null;
    if($blog_id == null){
        $act = "add";
        $data['created_by'] = $_SESSION['user_id'];
    }else{
        $act = "updat";
        $blog_info = $blog->getRowById($blog_id);
    }


    if(isset($_FILES['image']) && $_FILES['image']['error'] ==0){

        $image_name = uploadImage($_FILES['image'], "blog");
        if($image_name){
            $data['image'] = $image_name;

            if($blog_info[0]->image != null){
                deleteImage($blog_info[0]->image, 'blog');
            }
        }
    }

    //$row_id = $blog->insertData($data);

    if($act == 'add'){
        $row_id = $blog->insertData($data);
    }else{
        $row_id = $blog->updateRowById($data, $blog_id);
    }

    if($row_id){
        redirect("../blog.php", "success", "Blog ".$act. "ed successfully.");
    }else{
        redirect("../blog.php", "error", "Sorry there was a problem while ".$act."ing blog.");
    }

}elseif(isset($_GET['id']) && !empty($_GET['id'])){
    $id = (int)$_GET['id'];
    if ($id <=0){
        redirect("../blog.php", "error", "Sorry, cateogry ID is invalid");
    }
    $cat_info = $blog->getRowById($id);
    if(!$cat_info){
        redirect("../blog.php", "error", "Sorry, cateogry might have been deleted or does not exist.");
    }

    $status = $blog->deleteRowById($id);

    if($status){
        if($cat_info[0]->image != null){
            deleteImage($cat_info[0]->image, "blog");
        }
        //delete success
        redirect("../blog.php", "success", "Blog deleted successfully.");

    }else{
        // delete failure
        redirect("../blog.php", "error", "Sorry, there was a problem while deleting file");
    }
}
else{
    redirect("../blog-form.php", "error", "Please fill the form first");
}