<?php
require_once "../config/init.php";
$_title = "Advertisement form,".CMS_SITE_TITLE;
require_once "inc/header.php";
require "inc/checklogin.php";

$advertisement = new Advertisement;
if(isset($_GET['id']) && !empty($_GET['id'])) {
    $id = (int)$_GET['id'];
    if ($id <= 0) {
        redirect("advertisement.php", "error", "Sorry, Advertisement ID is invalid");
    }
    $ads_info = $advertisement->getRowById($id);
    if (!$ads_info) {
        redirect("advertisement.php", "error", "Sorry, Advertisement might have been deleted or does not exist.");
    }
}
?>

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include "inc/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <?php include "inc/top-nav.php"; ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">
                    Advertisement Form
                    <a href="advertisement-form.php" class="bt btn-success btn-sm float-right">
                        <i class="fa fa-plus">Advertisement Form</i>
                    </a>
                </h1>
                <?php echo flash(); ?>
                <div class="row">
                    <div class="col-12">
                        <form action="process/advertisement.php" method="post" enctype="multipart/form-data" class="form">
                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Title:</label>
                                <div class="col-sm-12 col-md-9">
                                    <input type="text" name="title" value="<?php echo @$ads_info[0]->title ?>" required placeholder="Enter Advertisement Title" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Link:</label>
                                <div class="col-sm-12 col-md-9">
                                    <input type="link" name="link" required class="form-control form-control-sm" value="<?php echo  @$ads_info[0]->link; ?>" placeholder="Enter Advertisement Link Here.">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3"> End Date:</label>
                                <div class="col-sm-12 col-md-9">
                                    <input type="date" name="end_date" required class="form-control form-control-sm" value="<?php echo  @$ads_info[0]->end_date; ?>" placeholder="Enter enter Date Link Here.">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Position:</label>
                                <div class="col-sm-12 col-md-9">
                                    <select name="position" id="status" class="form-control form-control-sm" required>
                                        <option value="" selected disabled>--Select Any Position--</option>
                                        <?php
                                        foreach ($ads_pos as $db_col_value => $ads_position){
                                        ?>
                                        <option value="<?php echo $db_col_value; ?>"
                                            <?php echo isset($ads_info[0]) && $ads_info[0]->position == $db_col_value ? 'selected' : '' ?>> <?php echo $ads_position; ?></option>

                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">status:</label>
                                <div class="col-sm-12 col-md-9">
                                    <select name="status" id="status" class="form-control form-control-sm" required>
                                        <option value="active" <?php echo (isset($ads_info) && $ads_info[0]->status == 'active') ? 'selected': '' ?> >Published</option>
                                        <option value="inactive" <?php echo (isset($ads_info) && $ads_info[0]->status == 'inactive') ? 'selected': '' ?> >Un-published</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-12 col-md-3">Image:</label>
                                <div class="col-sm-12 col-md-9">
                                    <input type="file" name="image" accept="image/*" class="form-control form-control-sm">
                                </div>

                                <div class="cod-sm-12 col-md-2">
                                    <?php
                                    if(isset($ads_info) && !empty($ads_info[0]->image) && file_exists(UPLOAD_DIR.'/advertisement/'.$ads_info[0]->image)){
                                        ?>
                                        <img src="<?php echo UPLOAD_URL.'/advertisement/'.$ads_info[0]->image ?>" alt="" class="img img-fluid" width="100" height="100" style="float: right">
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="offset-md-3 col-sm-12 col-md-9">
                                    <input type="hidden" name="ads_id" value="<?php echo @$ads_info[0]->id; ?>">
                                    <button class="btn-sm btn-danger" type="reset">
                                        <i class="fa fa-items"></i>Reset
                                    </button>

                                    <button class="btn-sm btn-success" type="submit">
                                        <i class="fa fa-paper-plane"></i>Submit
                                    </button>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php include "inc/copyright.php"; ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>



<?php include_once 'inc/footer.php'; ?>
