<?php
final class Advertisement extends  Database{
    use DataTraits;

    public function __construct()
    {
        parent::__construct();
        $this->table = 'advertisements';
    }
}
