<?php

    trait DataTraits{
    public function updateRowById($data, $row_id){
        $param = array(
            'where' => array(
                'id' => $row_id
            )

        );
        $status = $this->update($data, $param); // it returns true or false
        if($status){
            return $row_id;
        }else{
            return false;
        }
    }

    public function insertData($data, $is_debug = false){
        return $this->insert($data, $is_debug);
    }

    public function getAllRows($is_debug = false){
        return $this->select(array(), $is_debug);
    }

    public function getRowById($id){
        $param = array(
            'where' => array(
                'id'    => $id
            )
        );
        return $this->select($param);
    }

    public function deleteRowById($id){
        $param = array(
            'where' => array(
                'id'    => $id
            )
        );
        return $this->delete($param);
    }

}