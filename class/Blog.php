<?php

final class Blog extends Database{
    use DataTraits;

    public function  __construct()
    {
        parent::__construct();
        $this->table = "blogs";
    }

    public function getAllBlogs()
    {
        /*
         * SELECT blogs.id, blogs.title, blogs.summary, blogs.status, blogs.created_at
         * categories.title as category_title
         * FROM
         * blogs
         * LEFT JOIN categories ON categories.id = blogs.category_id
         * */

        $param = array(
          'fields'  =>array(
              "blogs.id",
              "blogs.title",
              "blogs.summary",
              "blogs.image",
              "blogs.status",
              "blogs.created_at",
              "categories.title as category_title"
          ),
          "leftjoin"   => "categories ON categories.id = blogs.category_id"
        );
        return $this->select($param);
    }
}