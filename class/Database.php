<?php
abstract class Database
{
    private $conn;

    protected $sql = null;
    protected $stmt = null;
    protected $table = null;

    public function __construct()
    {
        try {

            //$this->conn = new PDO("mysql:host=localhost;dbname=core_php;", "root", "");
            $this->conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";", DB_USER, DB_PASSWORD);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->sql = "SET NAMES utf8";
            $this->stmt = $this->conn->prepare($this->sql);
            $this->stmt->execute();

        } catch (PDOException $e) {
            //2020-10-07 18:38, (PDO, Connection): connection error message
            $msg = date("Y-m-d H:i") . ", (PDO, Connection):" . $e->getMessage() . " /r/n";
            error_log($msg, 3, ERROR_LOG);
        } catch (Exception $e) {
            $msg = date("Y-m-d H:i") . ", (General, Connection):" . $e->getMessage() . " /r/n";
            error_log($msg, 3, ERROR_LOG);
        }
    }

    final protected function select($args = array(), $debug = false)
    {
        try{
            /*
             * SELECT <fields> FROM <table>
             * LEFT JOIN <table1> ON <condition>
             * WHERE <clause>
             * GROUP BY <condition>
             * ORDER BY <column> <ASC/DESC>
             * LIMIT <index>, <count>
             *
             * SELECT id, email, password
             * WHERE email =:email AND status = :status
             * */

            $this->sql = "SELECT ";

            if(isset($args['fields'])){
                if(is_string($args['fields'])){
                    $this->sql .= $args['fields'];
                }else{
                    $this->sql .= implode(", ", $args['fields']);
                }
            }else{
                $this->sql .= " * ";
            }

            $this->sql .= " FROM ";
            if(!$this->table || empty($this->table)){
                throw new Exception(("Table not define."));
            }

            $this->sql .= $this->table;

            /* LEFT JOIN STARTS */
            if(isset($args['leftjoin']) && !empty($args['leftjoin'])){
                $this->sql .= " LEFT JOIN ".$args['leftjoin'];
            }
            /* LEFT JOIN ENDS */

            /*Where Clause Starts*/
            if(isset($args['where'])){
                if(is_string($args['where'])){
                    $this->sql .= " WHERE ".$args['where'];
                }else{
                    //associative aaray aauxa
                    $temp  = array();
                    foreach ($args['where'] as $column_name => $value){
                        $str = $column_name." = :".$column_name;
                        $temp[] = $str;
                    }
                    $this->sql .= " WHERE ";
                    $this->sql .= implode(" AND ", $temp);
                }
            }

            /*Where Clause Ends*/

            /* Group By Clause Start */
            /* Group By Clause End */

            /* Order By Clause Start */
            /* Order By Clause End */

            /* Limit Conditions Start */
            /* Limit Conditions End */

            if($debug){
                debug($args);
                debug($this->sql, true);
            }

            $this->stmt = $this->conn->prepare($this->sql);

            if(isset($args['where']) && !empty($args['where']) && is_array($args['where'])){
                foreach ($args['where'] as $column_name => $value){
                    if(is_int($value)){
                        $param = PDO::PARAM_INT;
                    }elseif(is_bool($value)){
                        $param = PDO::PARAM_BOOL;
                    }else{
                        $param = PDO::PARAM_STR;
                    }

                    $this->stmt->bindValue(":".$column_name, $value, $param);
                }
            }

            $this->stmt->execute();
            return $data = $this->stmt->fetchAll(PDO::FETCH_OBJ);

            //debug($args);
           // debug($this->sql);

        }catch (PDOException $e) {
            //2020-10-07 18:38, (PDO, SELECT): connection error message
            $msg = date("Y-m-d H:i").", (PDO, SELECT): ".$e->getMessage()." \r\n";
            error_log($msg, 3, ERROR_LOG);
        } catch (Exception $e) {
            $msg = date("Y-m-d H:i") . ", (General, SELECT):" . $e->getMessage()." \r\n";
            error_log($msg, 3, ERROR_LOG);
        }
    }

    final protected function insert($data = array(), $debug = false)
    {
        try{
            /*
             * INSERT INTO <table> SET
             * column_name_1 = :key_1
             * column_name_2 = :key_2
             * ................,
             * column_name_n = :key_n
             * */

            $this->sql = "INSERT INTO ";

            if(!$this->table || empty($this->table)){
                throw new Exception(("Table not defined."));
            }

            $this->sql .= $this->table." SET ";

            /* data binding STARTS */
            if(!isset($data) || empty($data)){
                throw new Exception("Data not defined to update.");
            }else{
                if(is_string($data)){
                    $this->sql .= $data;
                }else{
                    $tmp = array();
                    foreach ($data as $column_name => $value){
                        $str = $column_name." = :_".$column_name;
                        $tmp[] = $str;
                    }
                    $this->sql .= implode(" , ", $tmp);
                }
            }
            /* data binding ENDS */

            if($debug){
                debug($data);
                debug($this->sql, true);
            }

            $this->stmt = $this->conn->prepare($this->sql);

            /* Data Binding */
            if(isset($data) && !empty($data) && is_array($data)){
                foreach ($data as $column_name => $value){
                    if(is_int($value)){
                        $param = PDO::PARAM_INT;
                    }elseif(is_bool($value)){
                        $param = PDO::PARAM_BOOL;
                    }else{
                        $param = PDO::PARAM_STR;
                    }
                    $this->stmt->bindValue(":_".$column_name, $value, $param);
                }
            }

             $this->stmt->execute();
            return $this->conn->lastInsertId();

        }catch (PDOException $e) {
            //2020-10-07 18:38, (PDO, SELECT): connection error message
            $msg = date("Y-m-d H:i").", (PDO, INSERT): ".$e->getMessage()." \r\n";
            error_log($msg, 3, ERROR_LOG);
        } catch (Exception $e) {
            $msg = date("Y-m-d H:i") . ", (General, INSERT):" . $e->getMessage()." \r\n";
            error_log($msg, 3, ERROR_LOG);
        }

    }
    final protected function update($data = array(), $args = array(), $debug = false)
    {
        try{
            /*
             * UPDATE <table> SET
             * column_name_1 = :key_1
             * column_name_2 = :key_2
             * ................,
             * column_name_n = :key_n
             * WHERE <clause>
             * UPDATE users SET remember_token = :remember_token WHERE id = :id
             * UPDATE users SET email = :_email WHERE email = :email
             * */

            $this->sql = "UPDATE ";

            if(!$this->table || empty($this->table)){
                throw new Exception(("Table not defined."));
            }

            $this->sql .= $this->table." SET ";

            /* data binding STARTS */
            if(!isset($data) || empty($data)){
                throw new Exception("Data not defined to update.");
            }else{
                if(is_string($data)){
                    $this->sql .= $data;
                }else{
                    $tmp = array();
                    foreach ($data as $column_name => $value){
                        $str = $column_name." = :_".$column_name;
                        $tmp[] = $str;
                    }
                    $this->sql .= implode(" , ", $tmp);
                }
            }
            /* data binding ENDS */

            /*Where Clause Starts*/
            if(isset($args['where'])){
                if(is_string($args['where'])){
                    $this->sql .= " WHERE ".$args['where'];
                }else{
                    //associative aaray aauxa
                    $temp  = array();
                    foreach ($args['where'] as $column_name => $value){
                        $str = $column_name." = :".$column_name;
                        $temp[] = $str;
                    }
                    $this->sql .= " WHERE ";
                    $this->sql .= implode(" AND ", $temp);
                }
            }


            if($debug){
                debug($args);
                debug($this->sql, true);
            }

            $this->stmt = $this->conn->prepare($this->sql);

            /* Where clause Binding */
            if(isset($data) && !empty($data) && is_array($data)){
                foreach ($data as $column_name => $value){
                    if(is_int($value)){
                        $param = PDO::PARAM_INT;
                    }elseif(is_bool($value)){
                        $param = PDO::PARAM_BOOL;
                    }else{
                        $param = PDO::PARAM_STR;
                    }

                    $this->stmt->bindValue(":_".$column_name, $value, $param);
                }
            }

            /* Where clause Binding Ending */

            /* Where clause Binding */
            if(isset($args['where']) && !empty($args['where']) && is_array($args['where'])){
                foreach ($args['where'] as $column_name => $value){
                    if(is_int($value)){
                        $param = PDO::PARAM_INT;
                    }elseif(is_bool($value)){
                        $param = PDO::PARAM_BOOL;
                    }else{
                        $param = PDO::PARAM_STR;
                    }

                    $this->stmt->bindValue(":".$column_name, $value, $param);
                }
            }

            /* Where clause Binding Ending */
            return $this->stmt->execute();

        }catch (PDOException $e) {
            //2020-10-07 18:38, (PDO, SELECT): connection error message
            $msg = date("Y-m-d H:i").", (PDO, UPDATE): ".$e->getMessage()." \r\n";
            error_log($msg, 3, ERROR_LOG);
        } catch (Exception $e) {
            $msg = date("Y-m-d H:i") . ", (General, UPDATE):" . $e->getMessage()." \r\n";
            error_log($msg, 3, ERROR_LOG);
        }
    }
    final protected function delete($args = array(), $debug =false)
    {
        try{
            /*
             * DELETE FROM <table>
             * WHERE <clause>
             * */

            $this->sql = "DELETE FROM  ";

            if(!$this->table || empty($this->table)){
                throw new Exception(("Table not define."));
            }

            $this->sql .= $this->table;

            /*Where Clause Starts*/
            if(isset($args['where'])){
                if(is_string($args['where'])){
                    $this->sql .= " WHERE ".$args['where'];
                }else{
                    //associative aaray aauxa
                    $temp  = array();
                    foreach ($args['where'] as $column_name => $value){
                        $str = $column_name." = :".$column_name;
                        $temp[] = $str;
                    }
                    $this->sql .= " WHERE ";
                    $this->sql .= implode(" AND ", $temp);
                }
            }
            /*Where Clause Ends*/

            if($debug){
                debug($args);
                debug($this->sql, true);
            }

            $this->stmt = $this->conn->prepare($this->sql);

            if(isset($args['where']) && !empty($args['where']) && is_array($args['where'])){
                foreach ($args['where'] as $column_name => $value){
                    if(is_int($value)){
                        $param = PDO::PARAM_INT;
                    }elseif(is_bool($value)){
                        $param = PDO::PARAM_BOOL;
                    }else{
                        $param = PDO::PARAM_STR;
                    }

                    $this->stmt->bindValue(":".$column_name, $value, $param);
                }
            }

            return $this->stmt->execute();

        }catch (PDOException $e) {
            //2020-10-07 18:38, (PDO, SELECT): connection error message
            $msg = date("Y-m-d H:i").", (PDO, DELETE): ".$e->getMessage()." \r\n";
            error_log($msg, 3, ERROR_LOG);
        } catch (Exception $e) {
            $msg = date("Y-m-d H:i") . ", (General, DELETE):" . $e->getMessage()." \r\n";
            error_log($msg, 3, ERROR_LOG);
        }
    }
}