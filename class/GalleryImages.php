<?php

final class GalleryImages extends Database{
    use DataTraits;

    public function  __construct()
    {
        parent::__construct();
        $this->table = "galleries_images";
    }


    public function getImageByGalleryId($id)
    {
        $param = array(
          'where'   => array(
              'gallery_id'  => $id
          )
        );
        return $this->select($param);
    }

    public function deleteImageByName($del_image)
    {
        $param = array(
            'where' =>array(
                'image_name'    =>$del_image
            )
        );
        return $this->delete($param);
    }
}