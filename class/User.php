<?php
final class User extends Database
{
    use DataTraits;
    public function __construct()
    {
        $this->table = "users";
        parent::__construct();
    }

    public function getUserByEmail($email, $is_debug = false){
        // "SELECT * (email, id, password)FROM users WHERE email = :email "
        $param = array(
          //'fields'    => ["id", "email", "password"],
           //'fields'    => "id, email, password",
            //'where'     => " email = '".$email."' AND status = 'status'"
            'where'     => [
            'email' => $email,
            // 'status'=> 'active',
        ]
        );
        return $this->select($param);
    }

    public function getUserByCookie($remember_token, $is_debug = false){
        $param = array(
          'where'   => [
              'remember_token' =>$remember_token
          ]
        );
        return $this->select($param, $is_debug);
    }

    public function getUserByEmailRaw($email){
        $this->sql = "SELECT * FROM users WHERE email = '".$email."'";
        return $this->runRaw();
    }

    public function getUserByIdRaw($id)
    {
        $this->sql = "SELECT * FROM users WHERE id = ".$id;
        return $this->runRaw();
    }

    /*public function getAllUsers()
    {
        $this->sql = "SELECT * FROM users";
        return $this->runRaw();
    }*/
    public function getAllUsers()
    {
       $param = array(
           'where'  => " id != ".$_SESSION['user_id']
       );
       return $this->select($param);
    }

}