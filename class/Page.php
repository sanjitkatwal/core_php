<?php

final class Page extends Database{
    use DataTraits;

    public function  __construct()
    {
        parent::__construct();
        $this->table = "pages";
    }
}