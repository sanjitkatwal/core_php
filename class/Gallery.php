<?php

final class Gallery extends Database{
    use DataTraits;

    public function  __construct()
    {
        parent::__construct();
        $this->table = "galleries";
    }


    public function getActiveCategory()
    {
        $param = array(
            'where' => array(
                'status'    => 'active'
            )
        );
        return $this->select($param);
    }
}