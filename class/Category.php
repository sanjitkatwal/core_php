<?php

final class Category extends Database{
    use DataTraits;

    public function  __construct()
    {
        parent::__construct();
        $this->table = "categories";
    }


    public function getActiveCategory()
    {
        $param = array(
            'where' => array(
                'status'    => 'active'
            )
        );
        return $this->select($param);
    }
}